import { PageProps } from 'gatsby'
import React from 'react'

const NotFound: React.FC<PageProps> = () => (
  <main>
    <p>Sorry, page not found!</p>
  </main>
)

export default NotFound
